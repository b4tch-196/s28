// db.users.insertOne({
//     
//     "username": "carljosephong",
//     "password": "cjong"
//   
// })

// db.users.insertOne({
// 
//     "username": "gokuson",
//     "password": "over9000"
// })

//insert multiple documents at once

// db.users.insertMany(
//     [
//         {
//             "username": "pablo123",
//             "password": "123paul"
//         },
//         {
//             "username": "pedro99",
//             "password": "iampeter99"
//         }
//     ]
// )
//         
// db.products.insertMany(
//     [
//        {
//           "name": "sword",
//           "desciption": "shortsword",
//           "price": 50 
//        },
//        {
//           "name": "bow",
//           "desciption": "shortbow",
//           "price": 25 
//        },
//        {
//           "name": "staff",
//           "desciption": "longstaff",
//           "price": 10 
//        }
//     ]    
// )

// Read/Retrieve
// db.collection.find() - return/find all documents in the collection.
// db.users.find()

// db.collections.find({"criteria":"value"}) - returns/find all documents
// db.users.find({"username":"pedro99"})

// db.cars.insertMany(
//     [
//         {
//             "name": "Vios",
//             "brand": "Toyota",
//             "type": "sedan",
//             "price": 1500000
//         },
//         {
//             "name": "Tamaraw",
//             "brand": "Toyota",
//             "type": "auv",
//             "price": 750000
//         },
//         {
//             "name": "City",
//             "brand": "Honda",
//             "type": "sedan",
//             "price": 160000
//         }
//     ]
// )

// db.cars.find({"type":"sedan"})
// db.cars.find({"brand":"Toyota"})

// db.collection.findOne({}) - find/return the first item/document 
// in the collection.
// db.cars.findOne({})

// db.collection.findOne({"criteria":"value}) - find/return 
// the first item/document that matches the criteria
// db.cars.findOne({"type":"sedan"})
// db.cars.findOne({"brand":"Toyota"})
// db.cars.findOne({"brand":"Honda"})

// Update
// db.collection.updateOne({"criteria":"value"},{$set:{"fieldToBeUpdated":"Updated Value"}})
// Allows us to update the first item that matches our criteria
// db.users.updateOne({"username":"pedro99"},{$set:{"username":"peter1999"}})

// db.collection.updateOne({},{$set:{"fieldsToBeUpdated":"Updated Value"}})
// Allows us to update the first item in the collection
// db.users.updateOne({},{$set:{"username":"updatedUsername"}})

// If the field being updated does not yet exist, mongodb will instead add that field
// into the document.
// db.users.updateOne({"username":"pablo123"},{$set:{"isAdmin":true}})

// db.collection.updateMany({},{$set:{"fieldToBeUpdated":"Update Value"}})
// db.users.updateMany({},{$set:{"isAdmin":true}})

// db.collection.updateMany({"criteria":"value"},{$set:{"fieldToBeUpdated":"Updated Value"}})
// Allows us to update all items that matches our criteria
// db.cars.updateMany({"type":"sedan"},{$set:{"price":1000000}})

// Delete

// db.collection.deleteOne({}) - deletes first item in collection
// db.products.deleteOne({})
// db.collection.deleteOne({"criteria":"criteria"})
// deletes first item that matches criteria
// db.cars.deleteOne({"brand":"Toyota"})

// db.collection.deleteMany({"criteria":"value"})
// deletes all items that matches the criteria
// db.users.deleteMany({"isAdmin":true})

// db.collection.deleteMany({})
// delete all documents in a collection
// db.products.deleteMany({})
// db.cars.deleteMany({})
